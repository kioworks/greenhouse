const express = require('express')
const session = require('express-session');
const router = express.Router();
const bcrypt = require('bcryptjs');//hash password
const passport = require('passport');
const nodemailer = require('nodemailer');// to send emails from your server
const flash = require('express-flash');
const mongoose = require('mongoose');
const { ensureAuthenticated } = require('../config/authguard');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mymongodb";
const Verification = require('../models/Verification'); // the file verification.js is imported and the value
//of the variable Verification is passed
const mailer = require('../misc/mailer'); // to be used to send the verification mail, require the mailer.js file in misc folder, the mailer,js file requires the mailer.js in configuration folder
const saveVerificationCodeToDB = require('../functions/saveVerificationCodeToDB');
const sendVerificationMailCloseAccount = require('../functions/sendVerificationMailCloseAccount');
const renderNavBar = require('../functions/renderNavBar')

// User model
const User = require('../models/User');
//to send email with a link to the email account of the user
router.post('/closeaccount', (req, res, next) => {
    const { email, password } = req.body;
    var hash = '';
    console.log('this is working for close account, email and password in the form are:');
    console.log(email);
    console.log(password);
    let errors = [];
    // Check required fields
    if (email && password) {
        User.findOne({ email: req.body.email, password: req.body.password })
            .then(user => {
                if(user !== undefined){
                    console.log(password);
                    hash = req.user.password;
                    userEmail = req.user.email;
                    console.log("hash:  " + hash + " password:  " + password + " email:  " + email + " userEmail:  " + userEmail);
                    bcrypt.compare(password, hash, (err, isMatch) => {
                        if (err) throw err;
                        if (isMatch && email === userEmail) {
                            console.log('user hashed password and email match with body password and userEmail ');
                            let randomHash = saveVerificationCodeToDB(userEmail);
                            let message = "Click the link below to close your account with Goodminton.";
                            sendVerificationMailCloseAccount(randomHash, userEmail, message);
                            console.log('link sent to close account!');
                            req.flash('success_msg', 'Your account related to the email: ' + userEmail +
                            ' will be closed as soon as you verify the link sent to your email.');
                             res.redirect('/users/login');
                            //see funktion emailVerificationLinkCloseAccount, will be called when the link in the email close account is clicked
                            
                        } 
                        else{
                            errors.push({ msg: 'Your password and email have to belong to the user, try again !' });
                            res.render('closeaccount', renderNavBar(req, errors))
                        }
                    });
                }
            })
    }
    else if (!email || !password ) {
        errors.push({ msg: "You have to write your email and password, this is mandatory !" });
        res.render('closeaccount', renderNavBar(req, errors))
    }
    else if (errors.length > 0) {
        res.render('closeaccount', renderNavBar(req, errors))
    }
});
    

module.exports = router;