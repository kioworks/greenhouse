const express = require('express')
const router = express.Router();
const { ensureAuthenticated } = require('../config/authguard');

const Project = require('../models/Project');
const User = require('../models/User');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";



MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("mymongodb");

    dbo.collection("projects").find({}).toArray(function (err, result) {
        if (err) throw err;
        var tmpResult = JSON.stringify(result);
        for (i = 0; i < result.length; i++) {
            console.log(result[i].title);
        }
        db.close();
    })

});


module.exports = router;