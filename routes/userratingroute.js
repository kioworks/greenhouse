const express = require('express')
const router = express.Router();
const { ensureAuthenticated } = require('../config/authguard');
const renderNavBar = require('../functions/renderNavBar')


router.get('/userratingpage', ensureAuthenticated, (req, res) => {
    res.render('userratingpage', renderNavBar(req))

});

module.exports = router;