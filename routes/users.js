const express = require('express')
const session = require('express-session');
const router = express.Router();
const bcrypt = require('bcryptjs');//hash password
const passport = require('passport');
const nodemailer = require('nodemailer');// to send emails from your server
const flash = require('express-flash');
const mongoose = require('mongoose');
//to implement password reset
const LocalStrategy = require('passport-local').Strategy;
const bcryptnode = require('bcrypt-nodejs');
const async = require('async');
const crypto = require('crypto');
const { ensureAuthenticated } = require('../config/authguard');

// User in the arkiv models
const User = require('../models/User');

//Functions
const registrationFunction = require('../functions/registrationFunction');
const checkIfVerifiedOnLogin = require('../functions/checkIfVerifiedOnLogin');
const emailVerificationLink = require('../functions/emailVericifationLink');
const saveVerificationCodeToDB = require('../functions/saveVerificationCodeToDB');
const sendVerificationMail = require('../functions/sendVerificationMail');
//const emailVerificationLinkCloseAccount = require('../functions/emailVerificationLinkCloseAccount');


// Login page
router.get('/login', (req, res) => res.render('login'));

// Register page
router.get('/register', (req, res) => res.render('register'));

// profile sett
router.get('/profilesettings', ensureAuthenticated, (req, res) => res.render('profilesettings'));

router.get('/resend-verification', (req, res) => res.render('resend-verification'));

// Resend verification mail
router.post('/resend-verification', (req, res) => {  // Johan
    let email = req.body.email;
    console.log("Epostadress resend verification. email: " + email);
    let randomHash = saveVerificationCodeToDB(email);
    let message = "Klicka på länken för att aktivera ditt Greenhouse konto.";
    sendVerificationMail(randomHash, email, message);
    req.flash('success_msg', 'An email has been sent to you! Click on the included link to verify your account.');
    res.redirect('/users/resend-verification');
});

router.get('/verify', (req, res) => emailVerificationLink(req, res));

//forgot password in form action="/users/forgottenpassword"
router.get('/forgottenpassword', (req, res) => res.render('forgottenpassword'));

router.get('/verifycloseaccount', (req, res) => emailVerificationLinkCloseAccount(req, res));

router.get('/changepassword', (req, res) => res.render('changepassword', {
    Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper,
    companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
    lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
    address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
    devCompetence: req.user.devCompetence, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
}));

router.get('/accountsettings', (req, res) => res.render('accountsettings', {
    Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper,
    companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
    lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
    address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
    devCompetence: req.user.devCompetence, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
}));

router.get('/closeaccount', (req, res) => res.render('closeaccount', {
    Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper,
    companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
    lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
    address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
    devCompetence: req.user.devCompetence, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
}));


// Register handler
router.post('/register', (req, res) => registrationFunction(req, res));

// Login handler
router.post('/login', (req, res, next) => checkIfVerifiedOnLogin(req, res, next));

// Logout handle
router.get('/logout', (req, res) => {
    req.flash('success_msg', 'Your are logged out!');
    req.session.destroy();
    req.logout();
    res.redirect('/users/login');
});

//function to send email for reset password


module.exports = router;
