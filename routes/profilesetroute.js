const express = require('express')
const router = express.Router();
const { ensureAuthenticated } = require('../config/authguard');
const fs = require('fs');
// User model
const User = require('../models/User');
const DevCompetence = require('../models/DevCompetence');

//Functions 
const getCompetenceByIDAndUpdate = require('../functions/getCompetenceByIDAndUpdate');
const getCompetence = require('../functions/getCompetence');


router.get('/profilesettings', ensureAuthenticated, (req, res) => {

    //If req.user.profileTpe... return null set req.body.UserType to false
    if (req.user.profileTypeDeveloper === null) {
        req.body.Developer = false;
    } if (req.user.profileTypeOutsourcer === null) {
        req.body.Outsourcer = false;
    }

    //Getting the Competence from the user

    getCompetence(req, res, (err, comp) => {

        //If req.user.devCompetence returns true render ejs with devCompetence value of req.user.devCompetence
        if (req.user.devCompetence) {

            res.render('profilesettings', {
                Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper,
                companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
                lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
                address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
                devCompetence: comp.devCompetence, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
            })
        }
        //If function comp returns null from getCompetence set devCompetence in ejs to null
        if (comp === null) {
            res.render('profilesettings', {
                Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper, userId: req.user._id,
                companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
                lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
                address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
                devCompetence: null, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
            })
        }
    })
    //console.log(req.body.Developer + ' req.body ' + req.user.profileTypeDeveloper + ' req.user.dev')
});


// Profile settings post to db
router.post('/profilesettings', ensureAuthenticated, (req, res, next) => {

    var errors = [];
    var outsourcerToDB = req.body.Outsourcer;
    var developerToDB = req.body.Developer;

    //Setting userType to false if checkbox in html is not checked
    if (!developerToDB) {
        developerToDB = false;
    }
    if (!outsourcerToDB) {
        outsourcerToDB = false;
    }

    //Splitting the tags string to arrayToDB

    var stringToBeSplit = req.body.tagsArray
    var arrayToDB = stringToBeSplit.split(', ');

    //Then getting rid of all tags with value of whitespace.

    for (let i = 0; arrayToDB.length > i; i++) {
        if (arrayToDB[i] === '') {
            arrayToDB.splice(i, 1);
        }
    }

    //Check tag array length, if not at least 3 push error
    if (developerToDB && arrayToDB.length < 1) {
        errors.push({ msg: 'You must write at least 3 competences' });
    }


    //render profilesettings.ejs with errors
    if (errors.length > 0) {
        res.render('profilesettings', {
            errors,
            Outsourcer: outsourcerToDB, Developer: developerToDB, companyName: req.user.companyName,
            userNickName: req.user.userNickName, firstname: req.user.firstname, userId: req.user._id,
            lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
            address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
            devCompetence: arrayToDB, profileImage: req.user.profileImage,uploadCV: req.user.uploadCV

        });
    } else {  //Creating new competence if that property isnt occupated in user object
        if (!req.user.devCompetence) {
            const newCompetence = new DevCompetence({
                devCompetence: arrayToDB
            })

            newCompetence.save((err) => {
                if (err) console.log(err);
                else console.log("New competence added to DB")
            })

            User.findOneAndUpdate({ email: req.body.email }, {
                $set: {
                    profileTypeOutsourcer: outsourcerToDB, profileTypeDeveloper: developerToDB, userId: req.user._id,
                    companyName: req.body.companyName, userNickName: req.body.userNickName, firstname: req.body.firstname,
                    lastname: req.body.lastname, webpage: req.body.webpage, email: req.body.email, phonenr: req.body.phonenr,
                    address: req.body.address, postalCode: req.body.postalCode, countryName: req.body.countryName,
                    devCompetence: newCompetence, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
                }
            }).then((result) => {
                console.log('then   ', result)
                res.redirect('/dashboard');
                console.log('New competence added to the user.');
            }).catch(next);

        }
        //Else run function that updates the existing devComp property in user object
        else {
            getCompetenceByIDAndUpdate(req, res, arrayToDB);
            User.findOneAndUpdate({ email: req.body.email }, {
                $set: {
                    profileTypeOutsourcer: outsourcerToDB, profileTypeDeveloper: developerToDB, userId: req.user._id,
                    companyName: req.body.companyName, userNickName: req.body.userNickName, firstname: req.body.firstname,
                    lastname: req.body.lastname, webpage: req.body.webpage, email: req.body.email, phonenr: req.body.phonenr,
                    address: req.body.address, postalCode: req.body.postalCode, countryName: req.body.countryName,
                    profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
                }
            }).then((result) => {
                res.redirect('/dashboard');
                console.log('User updated.');
            }).catch(next);
        }
    }
});

module.exports = router;
