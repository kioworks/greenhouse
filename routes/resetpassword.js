const express = require('express')
const router = express.Router();
const bcrypt = require('bcryptjs');//hash password
const nodemailer = require('nodemailer');// to send emails from your server
//to implement password reset
const async = require('async');
const crypto = require('crypto');
const config = require('../config/mailer');
const renderNavBar = require('../functions/renderNavBar')



// User model
const User = require('../models/User');
//to send email with a link to the email account of the user
router.post('/forgottenpassword', (req, res, next) => {
    const { email } = req.body;
    let errors = [];
    // Check required fields
    if (email) {
        User.findOne({ email: req.body.email })
            .then(user => {
                if (user == undefined) {
                    // email does not exist in the database
                    console.log("email does not exist");
                    errors.push({ msg: 'Your email is wrong, write a valid email!' });
                    //not necesary nav bar  function in render metod, only errors
                    res.render('forgottenpassword', {
                        errors,
                    });
                } else {
                    req.flash('success_msg', 'The link for password reset will be sent to your : ' + email +
                        ' account. You are very welcome to login again with your new password.');
                    console.log('email sent?');
                    res.redirect('/users/login');
                    sendEmail(req, res, user, email);

                }
            })
            .catch(e => console.log(e))
    }
    else if (!email) {
        errors.push({ msg: "You forgot to write your email !" });
        //not necesary nav bar  function in render metod, only errors
        res.render('forgottenpassword', {
            errors,
        });
    }
    else if (errors.length > 0) {
         //not necesary nav bar  function in render metod, only errors
        res.render('forgottenpassword', {
            errors
        });
    }
});


//to change the password when the user is inside his account, after login
router.post('/changepassword', (req, res, next) => {
    console.log('this is working');
    const { password, newpassword, confirmnewpassword } = req.body;
    var hash = '';
    let errors = [];
    // Check required fields
    //to compare password and hashes bcrypt.compare()
    //bcrypt.compare(myPlaintextPassword, hash, function(err, res) {...})
    if (password && newpassword && confirmnewpassword) {
        User.findOne({ password: req.body.password })
            .then(user => {
                console.log(password);
                hash = req.user.password;
                email = req.user.email; //to send an email to the user that its password was changed
                console.log(req.user.password);
                bcrypt.compare(password, hash, (err, isMatch) => {
                    if (err) throw err;
                    if (isMatch) {
                        console.log('user hashed password match with body password');
                        hashingNewPasswordAndReplace(req, res, newpassword, user, email);
                    } else {
                        console.log("password is not from the user.")
                        errors.push({ msg: 'Your password does not belong to the user.' });
                        //function renderNavBar as function is passed as argument
                        res.render('changepassword', renderNavBar(req, errors))
                    }
                });
            })
            .catch(e => console.log(e))
        if (newpassword != confirmnewpassword) {
            errors.push({ msg: 'Password dos not match!' });
            res.render('changepassword', renderNavBar(req, errors))
        }
        // Check password length
        else if (newpassword.length < 6) {
            errors.push({ msg: 'New password must be at least 6 characters.' });
            res.render('changepassword', renderNavBar(req, errors))
        }
    }
    else if (!password || !newpassword || !confirmnewpassword) {
        errors.push({ msg: "You have to fill all the fields, this is mandatory !" });
        res.render('changepassword', renderNavBar(req, errors))
    }
    else if (errors.length > 0) {
        res.render('changepassword', renderNavBar(req, errors))
    }

});

//this will send an email with a link to be opened
function sendEmail(req, res, user, email) {

    async.waterfall([
        function (done) {
            crypto.randomBytes(20, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },

        function (token, done) {
            user.resetPasswordToken = token;
            user.resetPasswordExpires = Date.now() + 3600000;//1 hour valid token

            user.save(function (err) {
                done(err, token, user);
            });

        },

        function (token, user, done) {
            var smtpTransporter = nodemailer.createTransport({
                host: config.MAIL_HOST,
                //port: config.MAIL_PORT,
                //secure: config.MAIL_SECURE,
                auth: {
                    user: config.MAIL_USER,
                    pass: config.MAIL_PASSWORD
                },
                tls: {
                    rejectUnauthorized: false
                }
            });

            var mailOptions = {
                from: 'greenhousejava18@gmail.com',   // A copy will be sent to this email
                to: user.email,
                subject: 'Sending Email for Reset your Password',
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                    'http://' + req.headers.host + '/reset/' + token + '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n'
            }

            smtpTransporter.sendMail(mailOptions, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
        },
        function (err) {
            if (err) return next(err);
        }
        /* .catch(e => console.log(e)) */

    ])
}

router.get('/:token', function (req, res) {
    console.log('email token funkar');

    User.findOne({ resetPasswordToken: req.params.token })
        .then(user => {
            console.log(user)

            if (user !== null) {

                console.log(req.params.token);

                res.render('resetpassword');
            } else {
                console.log("token not found in DB.");
                res.render('forgottenpassword');
            }
            console.log("done");

        });
})

router.post('/:token', (req, res, next) => {

    let errors = [];
    if (req.body.newpassword && req.body.confirmnewpassword) {
        User.findOne({ resetPasswordToken: req.params.token })
            .then(user => {
                if (user == undefined) {

                    console.log("token does not exist");
                    errors.push({ msg: 'Invalid token!' });

                } else {
                    console.log('token exists');
                    console.log(user.password);
                    //function resetPassword will hash the new password and replace the new password in the Db
                    resetPassword(req, res, user.email, req.body.newpassword);
                    //Email confirmation for password reset
                    var smtpTransporter = nodemailer.createTransport({
                        host: config.MAIL_HOST,
                        //port: config.MAIL_PORT,
                        //secure: config.MAIL_SECURE,
                        auth: {
                            user: config.MAIL_USER,
                            pass: config.MAIL_PASSWORD
                        },
                        tls: {
                            rejectUnauthorized: false
                        }
                    });
                    var mailOptions = {
                        from: 'greenhousejava18@gmail.com',   // A copy will be sent to this email
                        to: user.email,
                        subject: 'Your have changed your password at Goodminton',
                        text: 'You are receiving this because you forgot your password and therefore a new password was created for your account with Goodminton.\n\n' +
                            'Please contact us if you did not change your password. .\n\n'

                    };
                    smtpTransporter.sendMail(mailOptions, function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log('Email sent: ' + info.response);
                        }
                    });
                }

            })
        /* .catch(e => console.log(e)) */
        if (req.body.newpassword != req.body.confirmnewpassword) {
            errors.push({ msg: 'Password dos not match!' });
            //not neccesary nav bar in render
            res.render('resetpassword', {
                errors,
            });
        }
        // Check password length
        else if (req.body.newpassword.length < 6) {
            errors.push({ msg: 'Password must be at least 6 characters.' });
            res.render('resetpassword', {
                errors,
            });
        }
    }
    else if (!req.body.newpassword || !req.body.confirmnewpassword) {
        errors.push({ msg: "You have to fill all the fields, this is mandatory !" });
        res.render('resetpassword', {
            errors,
        });
    }
    else if (errors.length > 0) {
        res.render('resetpassword', {
            errors
        });
    }
});

//this function will be called when the user is inloggad and voluntary will change its password
function hashingNewPasswordAndReplace(req, res, newpassword) {

    bcrypt.genSalt(10, (err, salt) =>
        bcrypt.hash(newpassword, salt, (err, hash) => {
            if (err) throw err;
            // Set password to hash
            console.log('this is for cryptering of new password')
            console.log("New password to be hashed:" + newpassword)
            newpassword = hash;
            console.log("Hash for new password: " + newpassword)
            User.findOneAndReplace({ password: req.user.password }, {
                $set: {
                    password: newpassword
                }
            }).then(user => {
                req.flash('success_msg', 'Your password has been changed. Do login with your new password.');
                res.redirect('/users/login');
                sendEmailForChangePassword(user);
                console.log('You have change your password, your new password is:' + newpassword);
            })
                .catch(e => console.log(e))
        }),
    )
}

//this function will be used when the user forgot its password and could not do inloggin
function resetPassword(req, res, email, newpassword) {

    bcrypt.genSalt(10, (err, salt) =>
        bcrypt.hash(newpassword, salt, (err, hash) => {
            if (err) throw err;
            // Set password to hash
            console.log('this is for cryptering of password')
            console.log(newpassword)
            newpassword = hash;
            console.log(newpassword)
            User.findOneAndReplace({ email: email }, {
                $set: {
                    password: newpassword
                }
            }).then(() => {
                req.flash('success_msg', 'Your password has been changed. Do login with your new password.');
                res.redirect('/users/login');
                console.log('Password updated.');
            })
                .catch(e => console.log(e))
        }),
    )
}

//this function will send an email when the user is inloggad and voluntary will change its password
function sendEmailForChangePassword(user) {
    //var email = req.user.email;
    console.log('the function for email for close account seems to work ')
    async.waterfall([
        //this function is important in order to save the user
        function (done) {
            user.save(function (err) {
                done(err, user);
            });
        },
        function (user) {
            var smtpTransporter = nodemailer.createTransport({
                host: config.MAIL_HOST,
                auth: {
                    user: config.MAIL_USER,  //Here will be the webb email in the host domain
                    pass: config.MAIL_PASSWORD
                },
                tls: {
                    rejectUnauthorized: false
                }
            });
            var mailOptions = {
                from: 'greenhousejava18@gmail.com',   // A copy will be sent to this email
                to: user.email,
                subject: 'You have changed your Password at Goodminton',
                text: 'You are receiving this because you (or someone else) have changed the password for your account with Goodminton.\n\n' +
                    'Please contact us if you have not change your password.\n\n'
            }

            smtpTransporter.sendMail(mailOptions, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
        },
        function (err) {
            if (err) return next(err);
        }
    ])
}

module.exports = router;