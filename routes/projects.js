const express = require('express');
const router = express.Router();

// Models
const Project = require('..//models/Project');

const createProject = require('../functions/createProject');
//const editProject = require('../functions/editProject');
const { ensureAuthenticated } = require('../config/authguard');

//GET-req, renders the form
router.get('/createproject', ensureAuthenticated, (req, res) => {

    res.render('createproject', {
        Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper, userId: req.user._id,
        companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
        lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
        address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
    })
});

//POST-req, creates a new project or saves it to draft
router.post('/createproject', ensureAuthenticated, (req, res) => {
    createProject(req, res);
});

router.get('/editproject/:id', ensureAuthenticated, (req, res) => {

    Project.findOne({ _id: req.params.id }, (err, thisProjectToEdit) => {
        console.log(req.params.id);
        console.log(thisProjectToEdit);
        res.render('editproject', {
            Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper, userId: req.user._id,
            companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
            lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
            address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
            profileImage: req.user.profileImage, title: thisProjectToEdit.title, description: thisProjectToEdit.description,
            payment: thisProjectToEdit.payment, requirements: thisProjectToEdit.requirements, devAmount: thisProjectToEdit.devAmount,
            uploadCV: req.user.uploadCV, devAcceptedList: thisProjectToEdit.devAcceptedList
        });
    });
});


router.post('/userratingpage', ensureAuthenticated, (req, res) => {
    editProject(req, res);
});
module.exports = router;


