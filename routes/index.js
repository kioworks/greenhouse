const express = require('express')
const router = express.Router();
const { ensureAuthenticated } = require('../config/authguard');

// Welcome page / Landingpage
router.get('/', (req, res) => res.render('login'));
//upload 
router.get('/photoupload', (req, res) => res.render('photoupload'));
router.get('/CVupload', (req, res) => res.render('CVupload'));

// Functions
const getProjects = require('../functions/getProjects');
const getCompetence = require('../functions/getCompetence');

// Models
const Project = require('..//models/Project');
const User = require('..//models/User');

const renderNavBarIndex = require('../functions/renderNavBarIndex');

// Dashboard start page
router.get('/dashboard', ensureAuthenticated, (req, res) => {
    // If user dose not exist redirects to profilesetting.
    if ((req.user.profileTypeOutsourcer == false) && (req.user.profileTypeDeveloper == false)) {
        res.redirect('/profilesetroute/profilesettings');
        console.log('User type not updated.');
    } else {
        // If user exist and profilesettings is uptodate
        var noMatch = null;
        if (req.query.search) {

            //const regex = new RegExp(escapeRegex(req.query.search), 'gi');
            // Get all projects from DB
            //Project.find({ $or: [{ title: regex }, { description: regex }, { requirements: regex }, { payment: regex }] }, function (err, allProjects) {
            Project.find({ $text: { $search: req.query.search } }, function (err, allProjects) {
                Project.find({ outSourcer: req.user._id }, function (err, allUserProjects) {

                    if (err) {
                        console.log(err);
                    } else {
                        if (allProjects.length < 1) {
                            noMatch = "No match with that query, please try again.";
                        }
                        //kallar function renderNavBarIndex
                        res.render('dashboard', renderNavBarIndex(req, res, allProjects, allUserProjects, noMatch))
                    }

                });
            });
        } else {
            // Get all projects from DB
            Project.find({}, function (err, allProjects) {
                Project.find({ outSourcer: req.user._id }, function (err, allUserProjects) {

                    if (err) {
                        console.log(err);
                    } else {
                        res.render('dashboard', renderNavBarIndex(req, res, allProjects, allUserProjects, noMatch))

                        // Delar upp $allProjects i sidor *******************************
                        // (Ett alternativ som kanske är snyggare och snabbare är att
                        //  använda SKIP och LIMIT i DB-anropet.) 
                    }

                });
                //}).limit(5).sort({ score: { $meta: 'textScore' } });
            }).sort({ date: -1 });
            //}).limit(5).sort({ date: -1 });
        }
    }
});

router.get('/match', ensureAuthenticated, (req, res) => {
    // If user exist and profilesettings is uptodate
    var noMatch = null;

    getCompetence(req, res, (err, comp) => {

        let matchArray = comp.devCompetence.join(' ');

        // Get all projects from DB compare with user competence

        Project.find({ $text: { $search: matchArray } }, { score: { $meta: 'textScore' } }, function (err, allProjects) {
            Project.find({ outSourcer: req.user._id }, function (err, allUserProjects) {

                if (err) {
                    console.log(err);
                } else {
                    if (allProjects.length < 1) {
                        noMatch = "No match with that query, please try again.";
                    }
                    res.render('dashboard', renderNavBarIndex(req, res, allProjects, allUserProjects, noMatch))
                }

            });
        }).limit(5).sort({ score: { $meta: 'textScore', date: -1 } });
        //}).limit(5).sort({ date: -1 });
    });
});


router.post('/dashboard/userprojects', ensureAuthenticated, (req, res) => {
    Project.find({ outSourcer: req.user._id }, (err, docs) => {
        res.send(docs);
    });
});

router.post('/dashboard/showallprojects', ensureAuthenticated, (req, res) => {
    Project.find((err, docs) => {
        res.send(docs);
    });
});

//Adding developers that applies for a project to that projects array
/* router.post('/dashboard/userjoinproject', ensureAuthenticated, (req, res) => {
    var projectID = req.body.id;

    Project.updateOne({ _id: projectID }, { $addToSet: { devApplied: req.user._id } }, function (err, projectToBeUpdated) {
        if (err) {
            console.log(err);
        } else {
            console.log('Updated devApplied array')
            req.flash('success_msg', 'Your have now applied for this project');
        }
    });
}); */


router.get('/dashboard/pingtoprojectowner/:id', ensureAuthenticated, (req, res) => {
    console.log('Test ping');
    console.log(req.params.id);
    Project.findOneAndUpdate({ _id: req.params.id }, {
        $addToSet: {
            devList: {
                userId: req.user._id, companyName: req.user.companyName, userNickName: req.user.userNickName,
                firstname: req.user.firstname, lastname: req.user.lastname, address: req.user.address,
                postalCod: req.user.postalCode, countryName: req.user.countryName, webpage: req.user.webpage,
                phonenr: req.user.phonenr, email: req.user.email, profileImage: req.user.profileImage,
                devCompetence: req.user.devCompetence, uploadCV: req.user.uploadCV
            }
        }
    }).then((result) => {
        res.redirect('/dashboard');
        console.log('User was pingd.');
    });
});


router.get('/dashboard/usercvmodal', ensureAuthenticated, (req, res) => {
    console.log('Test Modal server');
    User.find({}, (err, userCV) => {
        res.send(userCV);

        /*  userId: req.user._id, companyName: req.user.companyName, userNickName: req.user.userNickName,
             firstname: req.user.firstname, lastname: req.user.lastname, address: req.user.address,
                 postalCod: req.user.postalCode, countryName: req.user.countryName, webpage: req.user.webpage,
                     phonenr: req.user.phonenr, email: req.user.email, profileImage: req.user.profileImage,
                         devCompetence: req.user.devCompetence
  */
    }).then((result) => {
        //res.redirect('/dashboard');
        console.log('User CV-Modal OK.');
    });

});

router.get('/dashboard/useraccepted/:id', ensureAuthenticated, (req, res) => {
    var tmpParam = req.params.id;
    var tmpParamID = tmpParam.indexOf("-");
    var tmpProjectID = tmpParam.substr(0, tmpParamID);
    var tmpUserID = tmpParam.substr(tmpParamID + 1);
    console.log("str: ProjectID" + tmpProjectID + " och UserID" + tmpUserID);

    console.log('Test accept user ' + req.params.id);
    User.find({ _id: tmpUserID }, (err, user) => {
        Project.findOneAndUpdate({ _id: tmpProjectID }, {
            $addToSet: {
                devAcceptedList: {
                    accepted: user
                    /* userId: tmpUserID, companyName: req.user.companyName, userNickName: req.user.userNickName,
                    firstname: req.user.firstname, lastname: req.user.lastname, address: req.user.address,
                    postalCod: req.user.postalCode, countryName: req.user.countryName, webpage: req.user.webpage,
                    phonenr: req.user.phonenr, email: req.user.email, profileImage: req.user.profileImage,
                    devCompetence: req.user.devCompetence */

                }
            }
        }).then((result) => {
            res.redirect('/dashboard');
            console.log('User was accepted.');
        });
    });
});

/* 
function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
} */

module.exports = router;
