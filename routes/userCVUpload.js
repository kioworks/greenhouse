const multer = require('multer');
const path = require('path');
const express = require('express')
const routers = express.Router();
const { ensureAuthenticated } = require('../config/authguard');
const fs = require('fs');

// Project model
const Project = require('../models/Project');

const User = require('../models/User');
console.log("###inside photo.js");
/** Storage Engine */
const storageEngine = multer.diskStorage({
    destination: './public/CV',

    filename: function (req, file, fn) {
        console.log("### filesize " + file.size);
        console.log("### inside storageEngine Const. filename: ");
        let devCV = file.originalname;
        console.log("### inside filename: " + devCV);
        
        fn(null, req.user._id + '-' + file.fieldname + path.extname(devCV));
        
    }
});

routers.get('/profilesettings', ensureAuthenticated, (req, res) => {
    console.log("### inside PHOTO routers.get('/profilesettings',");

    Photo.find({}, ['path', 'caption'], { sort: { _id: -1 } }, function (err, photos) {
        console.log("id" + _id);

        if (req.user.profileTypeDeveloper === null) {
            req.body.Developer = false;
        }
        else if (req.user.profileTypeOutsourcer === null) {
            req.body.Outsourcer = false;
        }

    });

    res.render('profilesettings', {
        Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper,
        companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
        lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
        address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
        devCompetenceInput: req.user.devCompetenceInput, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
    })
    console.log("### req user profileimage: " + req.user.uploadCV);
});

const upload = multer({
    storage: storageEngine,
    limits: { fileSize: 2000000000000000000 },
    fileFilter: function (req, file, callback) {
        console.log("### inside multer");

        validateFile(file, callback);
    }
}).single('CV');


var validateFile = function (file, cb) {
    console.log("### inside validateFile");

    allowedFileTypes = /txt|docx|pdf|/;
    const extension = allowedFileTypes.test(path.extname(file.originalname).toLowerCase());
    const mimeType = allowedFileTypes.test(file.mimetype);
    if (extension && mimeType) {
        return cb(null, true);
    } else {
        cb("Invalid file type. Only TXT, DOCX and PDF file are allowed.")
    }
}

routers.post('/dashboard', ensureAuthenticated, (req, res) => {
    console.log("### inside routers.post('/dashboard', ensureAuthenticated,");

    console.log(req.body);
    var outsourcerToDB = req.body.Outsourcer;
    var developerToDB = req.body.Developer;


    if (!developerToDB) {
        developerToDB = false;
    }
    if (!outsourcerToDB) {
        outsourcerToDB = false;
    }

    upload(req, res, (err) => {
        console.log("### inside upload");
        if (err) {
            console.log("error: if (err) - UPLOAD")
        } else {
            if (req.file == undefined) {

                console.log("### file is undefined error")

            } else {
                var CVpath = "CV/" + req.file.filename;

                User.findOneAndUpdate({ email: req.user.email }, {
                    $set: {
                        uploadCV: CVpath
                    }
                }).then(console.log('Fungerar'), res.redirect('/projects/createproject')).catch(console.log('Catch'));

            } //cb(null, fullPath)
        }
    });


});




module.exports = routers;