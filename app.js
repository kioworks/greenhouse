// Johan 
const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
//to save session in mongoDB
const MongoStore = require('connect-mongo')(session);
//cookie-parser
const cookieParser = require('cookie-parser');

const localStrategy = require('passport-local').Strategy;
const app = express();

app.use(cookieParser());

// SPärrmekanism för att se om användaren är utloggad. Checka om sidan är giltig mot browserns cache

// Passport config
require('./config/passport')(passport);

// DB Config
const db = require('./config/keys').MongoURI;

// Connection to Mongo
mongoose.connect(db, { useNewUrlParser: true })
    .then(() => console.log('DB Connection Successful!'))
    .catch(err => console.log(err));


// EJS Layouts
app.use(expressLayouts);
app.set('view engine', 'ejs');



// Body parser
app.use(express.urlencoded({ extended: false }));


// Express session and mongostore
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({
        mongooseConnection: mongoose.connection,
        ttl: 2 * 24 * 60 * 60
    })
}));

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());
app.use(session({ secret: 'session secret key' }));

// Connect flash
app.use(flash());

// Global variabels
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
});

// no cache if logged out
app.use(function (req, res, next) {
    if (!req.user) {
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
    }
    next();
});

// CSS
app.use(express.static('./public'));
//app.use(express.static('uploads'));
app.use(express.static('./public/files'));




// Routes, js files
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/projects', require('./routes/projects'));
app.use('/profilesetroute', require('./routes/profilesetroute'));
app.use('/userratingroute', require('./routes/userratingroute'));
app.use('/resetpassword', require('./routes/resetpassword'));
app.use('/reset', require('./routes/resetpassword'));
app.use('/photoupload', require('./routes/photo'));
app.use('/CVupload', require('./routes/userCVupload'));


//------------------------image-------------------------



app.use('/closeaccount', require('./routes/closeaccount'))

const PORT = process.env.PORT || 5000;



app.listen(PORT, console.log(`Server started on port ${PORT}`));

// Armen
// git add .
// git commit -m "Armen"
// git push -u origin master
// --------------------------
// git pull origin master