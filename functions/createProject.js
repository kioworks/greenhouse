const Project = require('../models/Project')
const session = require('express-session');
const renderNavBar = require('../functions/renderNavBar')

module.exports = function (req, res) {

    const { title, description, payment, devAmount, tagsArray } = req.body;
    const outSourcer = req.user._id;

    var requirements = tagsArray.split(',');
    var errors = [];

    //Get submit buttons from their name tags
    req.body = JSON.parse(JSON.stringify(req.body));

    let createbutton = req.body.hasOwnProperty('createProjectButton');
    let saveButton = req.body.hasOwnProperty('savetodraftProjectButton');

    //check required fields
    if (!title || !description || !requirements || !devAmount) {
        errors.push({ msg: 'Please fill in all fiealds.' });
    }

    //Check how many reqs is filled in
    if (requirements.length < 3) {
        errors.push({ msg: 'Please fill in at least three skills.' });
    }
    //Error handler
    if (errors.length > 0) {
        console.log(errors);
        res.render('createproject', renderNavBar(req, errors))
    } else {

        // IF the create and publish button is clicked do this...
        if (createbutton) {

            // Creating and publishing project
            console.log("create")

            const newProject = new Project({
                title,
                description,
                requirements,
                payment,
                outSourcer,
                devAmount,
                projectImage: req.user.profileImage
            });

            // Save to DB
            newProject.save()
                .then(project => {
                    req.flash('success_msg', 'You have  created a project');
                    res.redirect('/dashboard');
                })
                .catch(err => console.log(err));
        }

        //IF save button is clicked do this....
        else if (saveButton) {
            // Creating and save project, not publish
            console.log("save")
            let statusActive = false;
            let statusSavedToDraft = true;
            const newProject = new Project({
                title,
                description,
                requirements,
                payment,
                outSourcer,
                statusActive,
                statusSavedToDraft,
                devAmount,
                projectImage: req.user.profileImage
            });
            // Save to DB
            newProject.save()
                .then(project => {
                    req.flash('success_msg', 'You have saved your project');
                    res.redirect('/dashboard');
                })
                .catch(err => console.log(err));
        }
    }

}