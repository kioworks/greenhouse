
//Models
const Verification = require('../models/Verification'); // Johan
const User = require('../models/User');

module.exports = (req, res) => {
    let email = "";

    Verification.findOne({ verificationCode: req.query.id })
        .then(verification => {
            if (verification) {
                console.log("Verificationcode found.");
                email = verification.email;
                console.log("User.updateOne" + email);
                User.updateOne({ email: email }, { accountStatus: true }, function (err, res) {
                    console.log("updateone " + res);
                });
                //res.redirect('/users/emailverified');
                req.flash('success_msg', 'Your account is verified and ready to use.');
                res.redirect('/users/login');

            } else {
                console.log("Verification code NOT found!!!");
                req.flash('error_msg', 'Your activation email link has expired! Fill in your email address to receive a new verification email.');

                res.redirect('/users/resend-verification');// **************************************************
            }
        });
}
