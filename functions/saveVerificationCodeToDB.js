const Verification = require('../models/Verification'); // Johan

module.exports = function (email) {

    var rand = Math.floor((Math.random() * 100000) + 99); // Skapar tillägg till länk i epost
    const newVerification = new Verification({
        email,
        verificationCode: rand
    });
    newVerification.save(function (err) {
        if (err) console.log("FEL Sparar ej Verification till databas");
        // saved!
    });
    return rand;
}