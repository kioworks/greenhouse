const mailer = require('../misc/mailer');

//argumenter are random link , email, and message
module.exports = function (rand, email, message) {
    const mailconfig = require('../config/mailer');
    var link = "http://localhost:5000/users/verifycloseaccount?id=" + rand;
    var html = message + `<br> <a href="` + link + `">Close account</a>`;
    var from = mailconfig.MAIL_USER;
    try {
        mailer.sendEmail(from, 'Close your account with Goodminton TEST de Los Matchos', email, html)
            .then(info => {
                console.log(info)
            })
            .catch(e => console.log(e))
    } catch (e) {
        console.log(e)
    }
}