//Models
const Verification = require('../models/Verification'); // Johan
const User = require('../models/User');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mymongodb";
const closedaccount = require('../models/closedaccount');
/* const closefunction= require('../functions/closefunction'); */


module.exports = (req, res) => {
    let email = "";

    User.findOne({ User: req.query.email , User:req.query.accountStatus})
        .then(User => {
            if (User) {

                email = User.email;
                accountStatus = User.accountStatus;
                profileTypeAdmin = User.profileTypeAdmin;
                profileTypeOutsourcer = User.profileTypeOutsourcer;
                profileTypeDeveloper =User.profileTypeDeveloper;
                profileTypeDevAndOutsourcer=User.profileTypeDevAndOutsourcer;
                companyName = User.companyName;
                userNickName = User.NickName;
                firstname = User.firstname;
                lastname = User.lastname;
                adress = User.address;
                postalCode =User.postalCode;
                countryName =User.countryName;
                webpage = User.webpage;
                phonenr =User.phonenr;
                console.log("User.updateOne" + email);
                console.log('this is function  closeAccount')
                var userAccountStatus = req.user.accountStatus;
                console.log("account status is: " + userAccountStatus);
                MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
                    console.log('this is mongo client connect')
                    var dbo = db.db("mymongodb");
                    //db.collection.updateOne(filter, update, options)
                    //https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/
                    dbo.collection("users").updateOne(
                       { email:email },
                        { $set: { accountStatus: false } }, function (err, res) {
                            if (err) throw err;
                            console.log("account status updated to false");
                        }
                    );
                    MongoClient.connect(url, function(err, db) {
                        if (err) throw err;
                        var dbo = db.db("mymongodb");
                        dbo.createCollection("closedaccount", function(err, res) {
                          if (err) throw err;
                          console.log("Collection created!");
                         // db.users.copyTo('closedaccount');
                         // dbo.collection('closedaccount').insertOne(user);
                         //  dbo.collection("users").findOne(
                         //    {email:email},



                      //   )
                         console.log(email);
                         dbo.collection('closedaccount').createIndex({"createdAt":1},{ expireAfterSeconds:60*60*24*30 } )
                         dbo.collection('closedaccount').insertMany([{email:email,accountStatus: false ,profileTypeAdmin
                            :
                            profileTypeAdmin,
                            profileTypeOutsourcer
                           :
                            profileTypeOutsourcer,
                            profileTypeDeveloper
                            :
                            profileTypeDeveloper,
                            profileTypeDevAndOutsourcer
                            :
                            profileTypeDevAndOutsourcer,
                            companyName
                            :
                            companyName,
                            userNickName
                            :
                            userNickName,
                            firstname
                            :
                            firstname,
                            lastname
                            :
                            lastname,
                            address
                            :
                            adress,
                            postalCode
                            :
                            postalCode,
                            countryName
                            :
                            countryName,
                            webpage
                            :
                            webpage,
                            phonenr
                            :
                            phonenr,
                            createdAt
                            :
                            new Date()
                        }],
                            function (err, res) {
                            if(err) throw err;
                            console.log("Number of documents inserted: " + res.insertedCount);


                            db.close();

                           MongoClient.connect(url, function(err, db) {
                            if (err) throw err;
                            var dbo = db.db("mymongodb");
                          console.log(email);

                           dbo.collection("users").deleteOne({"email": email }, function(err, obj) {
                          if (err) throw err;
                          console.log(obj.result.n + " document(s) deleted");

                         db.close();

                        })
                    })


                        })


                    })

                        });
                      });



               // db.close();
                /* User.updateOne({ email: email }, { accountStatus: false }, function (err, res) {
                    console.log("updateone " + res);
                }); */
                //res.redirect('/users/emailverified');
                req.flash('success_msg', 'Your account has been closed !. Just let us know how we can improve.');
                res.redirect('/users/login');

            } else {
                console.log("Verification code NOT found!!!");
                req.flash('error_msg', 'Your close account email link has expired! Fill in your email address to receive a new verification email.');

                res.redirect('/users/resend-verification');
            }
        });
}
