const DevCompetence = require('../models/DevCompetence');
const User = require('../models/User');

module.exports = function (req, res, updateData) {
    
    //find the devComp object with user object property of devCompetence wich is the devComp id.
    //Setting the devComp to the array (updateData) that we get from profilesettings
    DevCompetence.findOneAndReplace({ _id: req.user.devCompetence }, {

        $set: {
            devCompetence: updateData
        }

    }).then((result) => {
        
    }).catch(() => {
        
    });

}