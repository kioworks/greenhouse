const passport = require('passport');

//Models
const User = require('../models/User');

module.exports = (req, res, next) => {
    // Kontrollerar om kontot är verifierat
    const { firstname, lastname, email, email2, password, password2 } = req.body;

    var passportOptions = {
        successRedirect: '/dashboard',
        failureRedirect: '/users/login',
        failureFlash: true
    };
    
    User.findOne({ email: email })
        .then(user => {
            if (user) {
                console.log("Verification code found.")
                if (user.accountStatus == false) {
                    console.log("Account not activated! No verification code found.")
                    passportOptions.successRedirect = '/users/login';
                    req.flash('error_msg', 'Your account has not yet been verified! Use the verification mail that was sent you to activate your account, or <a href="/users/resend-verification">click here to resend verification mail.</a>');
                } else if (user.accountStatus == true) {
                    if (!user.profileTypeOutsourcer && !user.profileTypeDeveloper) {
                        passportOptions.successRedirect = '/profilesetroute/profilesettings';
                    }
                }
            } else {
                console.log(email + " not found in DB.")
            }
            console.log("passportoptions: " + passportOptions.successRedirect);
            passport.authenticate('local', passportOptions)(req, res, next);
        });
}
