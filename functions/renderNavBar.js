//detta funtion ska returnera alla properties to fill the navigation bar with the render method for close account
// res.render('closeaccount', renderNavBar(req, errors))
//res.render('changepassword', renderNavBar(req, errors))
//res.render('userratingpage', renderNavBar(req))

module.exports = (req, errors) => {
    const navBar = {
        errors, Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper, userId: req.user._id,
        companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
        lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
        address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
        devCompetence: req.user.devCompetence, profileImage: req.user.profileImage, uploadCV: req.user.uploadCV
    }

    return navBar;
}