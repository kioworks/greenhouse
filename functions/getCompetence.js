const DevCompetence = require('../models/DevCompetence');

module.exports = function (req, res, callback) {

    DevCompetence.findById(req.user.devCompetence, (err, docs) => {
        try {
            callback(err, docs);
        } catch (err) {
            console.log(err);
        }
    });

}