module.exports = (req, res, allProjects, allUserProjects, noMatch) => {
    const projectsPerPage = 5; // Antal projekt som visas per sida
    let pageNumber = req.query.page;
    if (!req.query.page) {
        pageNumber = 1;
    }

    let startSlice = pageNumber * projectsPerPage - projectsPerPage;
    let endSlice = startSlice + projectsPerPage;
    let projectsPaginated = allProjects.slice(startSlice, endSlice);
    const renderNavBarIndex = {
        projectsCount: allProjects.length, projects: projectsPaginated, projectsPerPage: projectsPerPage,
        projectsPageNumber: pageNumber, userProjects: allUserProjects, noMatch: noMatch,
        userId: req.user._id, Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper,
        companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
        lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
        address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
        devCompetence: req.user.devCompetence, profileImage: req.user.profileImage, projectsDone: req.user.projectsDone,
        uploadCV: req.user.uploadCV
    }

    return renderNavBarIndex;
}