const bcrypt = require('bcryptjs');
const User = require('../models/User');

//Functions
const saveVerificationCodeToDB = require('../functions/saveVerificationCodeToDB');
const sendVerificationMail = require('../functions/sendVerificationMail');


module.exports = function (req, res) {

    const { firstname, lastname, email, email2, password, password2 } = req.body;
    let errors = [];

    // Check required fileds
    if (!firstname || !lastname || !email || !email2 || !password || !password2) {
        console.log(req.body);
        errors.push({ msg: 'Please fill in all fields!' });
    }

    // Check if email match
    if (email !== email2) {
        errors.push({ msg: 'Email dos not match!' });
    }

    // Check password match
    if (password !== password2) {
        errors.push({ msg: 'Password dos not match!' });
    }

    // Check password length
    if (password.length < 6) {
        errors.push({ msg: 'Password must be at least 6 characters.' });
    }

    if (errors.length > 0) {
        res.render('register', {
            errors,
            firstname,
            lastname,
            email,
            email2,
            password,
            password2
        });
    }
    else {
        // Validation passed
        User.findOne({ email: email })
            .then(user => {
                if (user) {
                    // User exists!
                    errors.push({ msg: 'Email is already registered!' });
                    res.render('register', {
                        errors,
                        firstname,
                        lastname,
                        email,
                        email2,
                        password,
                        password2
                    });
                }
                else {
                    const newUser = new User({
                        accountStatus: false,
                        profileTypeAdmin: false,
                        profileTypeOutsourcer: false,
                        profileTypeDeveloper: false,
                        profileTypeDevAndOutsourcer: false,
                        companyName: '',
                        userNickName: '',
                        firstname,
                        lastname,
                        address: '',
                        postalCode: '',
                        countryName: '',
                        webpage: '',
                        phonenr: '',
                        email,
                        password,
                        profileImage: './profile-icon_default.png'
                    });
                    // Hash password
                    bcrypt.genSalt(10, (err, salt) => bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        // Set password to hashed
                        newUser.password = hash;
                        // Save user
                        newUser.save()
                            .then(user => {
                                // Spara randomHash till DB, collection:Verifications
                                let randomHash = saveVerificationCodeToDB(newUser.email);
                                let message = "Klicka på länken för att verifiera ditt konto!";
                                // Skicka verifikationsmejl till användaren
                                sendVerificationMail(randomHash, newUser.email, message);
                                //req.flash('success_msg', 'You are now registered and can logg in!');
                                req.flash('success_msg', 'An email has been sent to you! Click on the included link to verify your account.');
                                console.log('User saved!');
                                res.redirect('/users/login');
                            })
                            .catch(err => console.log(err));
                    }));
                }
            });
    }
}
