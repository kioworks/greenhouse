
const Project = require('../models/Project');
const User = require('../models/User');

module.exports = function (callback) {

    // Getting all projets from projects collection and passing them in callback to dashboard route in index.js
    Project.find({}, function (err, docs) {
        try {
            callback(err, docs);
        } catch(err) {
            console.log(err);
        }
    });
    
    /* getProjects: async (projects) => (await (() => (
        new Promise((resolve, reject) => {
    
            //go ahead and make the query...
            Project.find({}, (err, data) => {
                    if (err) {
                        console.log(err)
                    } else {
                        resolve(data);
                    }
                });
        })
        )))() */
}
