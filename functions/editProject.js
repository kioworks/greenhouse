const Project = require('../models/Project')
const session = require('express-session');


module.exports = function (req, res) {

    const { title, description, payment, devAmount, tagsArray } = req.body;

    const outSourcer = req.user._id;

    var requirements = tagsArray.split(',');
    var errors = [];

    //Get submit buttons from their name tags
    req.body = JSON.parse(JSON.stringify(req.body));

    let editbutton = req.body.hasOwnProperty('editProjectBtn');
    let saveButton = req.body.hasOwnProperty('savetodraftProjectButton');


    //check required fields
    if (!title || !description || !requirements || !devAmount) {
        errors.push({ msg: 'Please fill in all fiealds.' });
    }

    //Check how many reqs is filled in
    if (requirements.length < 3) {
        errors.push({ msg: 'Please fill in at least three skills.' });
    }
    //Error handler
    if (errors.length > 0) {
        console.log(errors);
        res.render('editproject/:id', {
            errors,
            Outsourcer: req.user.profileTypeOutsourcer, Developer: req.user.profileTypeDeveloper, userId: req.user._id,
            companyName: req.user.companyName, userNickName: req.user.userNickName, firstname: req.user.firstname,
            lastname: req.user.lastname, webpage: req.user.webpage, email: req.user.email, phonenr: req.user.phonenr,
            address: req.user.address, postalCode: req.user.postalCode, countryName: req.user.countryName,
            profileImage: req.user.profileImage, title: thisProjectToEdit.title, description: thisProjectToEdit.description,
            payment: thisProjectToEdit.payment, requirements: thisProjectToEdit.requirements,
            devAmount: thisProjectToEdit.devAmount, uploadCV: req.user.uploadCV, devAcceptedList: thisProjectToEdit.devAcceptedList
        });
    } else {

        // IF the create and publish button is clicked do this...
        if (editbutton) {

            // Creating and publishing project
            console.log("Edit")

            // Edit to DB

            //IF save button is clicked do this....
        } else if (saveButton) {
            // Editing and save project, not publish

            // Save to DB

        }
    }

}