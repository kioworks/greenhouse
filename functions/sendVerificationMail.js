const mailer = require('../misc/mailer');

module.exports = function (rand, email, message) {
    const mailconfig = require('../config/mailer');
    var link = "http://localhost:5000/users/verify?id=" + rand;
    var html = message + `<br> <a href="` + link + `">Verify</a>`;
    var from = mailconfig.MAIL_USER;
    try {
        mailer.sendEmail(from, 'Please verify email TEST', email, html)
            .then(info => {
                console.log(info)
            })
            .catch(e => console.log(e))
    } catch (e) {
        console.log(e)
    }
}
