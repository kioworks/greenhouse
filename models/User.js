const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
    accountStatus: {
        type: Boolean,
        required: true,
        default: false
    },
    profileTypeAdmin: {
        type: Boolean,
        required: false,
        default: false
    },
    profileTypeOutsourcer: {
        type: Boolean,
        required: false,
        default: false
    },
    profileTypeDeveloper: {
        type: Boolean,
        required: false,
        default: false
    },
    profileTypeDevAndOutsourcer: {
        type: Boolean,
        required: false,
        default: false
    },
    companyName: {
        type: String,
        required: false
    },
    userNickName: {
        type: String,
        required: false
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: false
    },
    postalCode: {
        type: String,
        required: false
    },
    countryName: {
        type: String,
        required: false
    },
    webpage: {
        type: String,
        required: false
    },
    phonenr: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    devCompetence: {
        type: Schema.Types.ObjectId,
        ref: 'DevCompetence',
        required: false
    },
    profileImage: {
        type: String,
        contenttype: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    },
    resetPasswordToken: {
        type: String,
        required: false
    },
    resetPasswordExpired: {
        type: Date,
        required: false
    },
    uploadCV:{
        type:  String,
        contenttype: String,
        required:false
    },
    projectsList: {
        type: [],
        required: false
    }
})

const User = mongoose.model('User', UserSchema);

module.exports = User;
