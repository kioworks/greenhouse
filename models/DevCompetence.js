const mongoose = require('mongoose');

const DevCompetenceSchema = new mongoose.Schema({
    devCompetence: {
        type: [],
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

const DevCompetence = mongoose.model('DevCompetence', DevCompetenceSchema);

module.exports = DevCompetence;