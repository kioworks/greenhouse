const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const projectSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    payment: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    requirements: {
        type: [],
        required: true
    },
    outSourcer: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    statusActive: {
        type: Boolean,
        required: true,
        default: true
    },
    statusPaused: {
        type: Boolean,
        required: false,
        default: false
    },
    statusSavedToDraft: {
        type: Boolean,
        required: true,
        default: false
    },
    projectImage: {
        type: String,
        contenttype: String,
        required: false
    },
    devAmount: {
        type: Number,
        required: false
    },
    devList: {
        type: [],
        required: false
    },
    devAcceptedList: {
        type: [],
        required: false
    }
});

const Project = mongoose.model('Project', projectSchema);

module.exports = Project;