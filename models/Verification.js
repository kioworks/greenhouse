const mongoose = require('mongoose');

const timeBeforeExpire = 60 * 60 * 24; // Sekunder innan documentet tas bort automatiskt

let VerificationSchema = mongoose.Schema({
    email: String,
    verificationCode: Number,
}, { timestamps: true });

VerificationSchema.index({ createdAt: 1 }, { expireAfterSeconds: timeBeforeExpire });

const Verification = mongoose.model('Verification', VerificationSchema);
module.exports = Verification;
