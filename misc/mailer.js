const mailer = require('nodemailer');

const config = require('../config/mailer');

var smtpConfig = {
   host: config.MAIL_HOST,
   //port: config.MAIL_PORT,
   //secure: config.MAIL_SECURE,
   auth: {
      user: config.MAIL_USER,
      pass: config.MAIL_PASSWORD
   },
   tls: {
      rejectUnauthorized: false
   }
};

/* var smtpConfig = {
   host: 'smtp.gmail.com',
   port: 587,
   secure: false,
   auth: {
      user: 'greenhousejava18@gmail.com',
      pass: 'java18greenhouse'
   },
   tls: {
      rejectUnauthorized: false
   }
}; */

/*
var smtpConfig1 = {
   host: 'smtp.gmail.com',
   port: 465,
   secure: true,
   auth: {
      user: 'losmatchos@gmail.com',
      pass: 'Roland2019'
   }
};

var smtpConfig2 = {
   host: 'smtp.live.com',
   port: 587,
   secure: true,
   auth: {
      user: "johanjames@hotmail.com",
      pass: "aracatI2003"
   }
};

var smtpConfig3 = {
   host: 'smtp.live.com',
   auth: {
      user: "johanjames@hotmail.com",
      pass: "aracatI2003"
   }
};
*/

const transport = mailer.createTransport(smtpConfig);




module.exports = {

   sendEmail(from, subject, to, html) {

      return new Promise((resolve, reject) => {

         transport.sendMail({ from, subject, to, html }, (err, info) => {

            if (err) reject(err);

            else resolve(info);

         });

      });

   }

}
