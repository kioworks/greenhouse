// toggle function for buttons for taskbar and taskcreated and contactlist for user
function OpenTasks() {
    //document.getElementById("taskSide").style.width = "385px";
    //document.getElementById("taskSide").style.display = "inline";
    document.getElementById("taskSide-outerframe").style.display = "flex"; // Johan
    document.getElementById("taskLeftCloseBtn").style.display = "inline";
    document.getElementById("taskRightOpenBtn").style.display = "none";
}

function closeTasks() {
    //document.getElementById("taskSide").style.width = "0px";
    //document.getElementById("taskSide").style.display = "none";
    document.getElementById("taskSide-outerframe").style.display = "none"; //Johans kod
    document.getElementById("taskLeftCloseBtn").style.display = "none";
    document.getElementById("taskRightOpenBtn").style.display = "inline";
    //document.getElementById("page-center-side").className = "col-sm-8";
}


function openTasksAndContacts() {
    document.getElementById("taskAndContactSide").style.width = "385px";
    document.getElementById("taskAndContactSide").style.display = "inline";

    document.getElementById("taskAndContactLeftCloseBtn").style.display = "none";
    document.getElementById("taskAndContactRightOpenBtn").style.display = "inline";
}

function closeTasksAndContacts() {
    document.getElementById("taskAndContactSide").style.width = "0px";
    document.getElementById("taskAndContactSide").style.display = "none";
    document.getElementById("taskAndContactLeftCloseBtn").style.display = "inline";
    document.getElementById("taskAndContactRightOpenBtn").style.display = "none";
}

// Gets all projects created by outsourcer
function getUserProjects(callback) {
    let error = null;
    $.ajax({
        method: 'POST',
        dataType: 'json',
        data: "data",
        jsonp: 'callback',
        url: '/dashboard/userprojects',
        success: function (data) {
            callback(error, data)
        }
    }).done(data => {
    }).catch(error => console.log(error));
}

// Gets all projects from DB
function getAllProjects(callback) {
    let error = null;
    $.ajax({
        method: 'POST',
        dataType: 'json',
        data: "data",
        jsonp: 'callback',
        url: '/dashboard/showallprojects',
        success: function (data) {
            callback(error, data)
        }
    }).done(data => {
    }).catch(error => console.log(error));
}

//------------ Eventlistnener for project buttons -------------------------
if (document.addEventListener) {
    document.addEventListener("click", handleClick, false);
}
else if (document.attachEvent) {
    document.attachEvent("onclick", handleClick);
}

function handleClick(event) {

    event = event || window.event;
    event.target = event.target || event.srcElement;

    var element = event.target;

    // Climbs up the document tree from the target of the event
    while (element) {
        if (element.nodeName === "BUTTON" && /usersprojects/.test(element.className)) {
            // The user clicked on a <button> or clicked on an element inside a <button>
            showThisUserProject(element);
            break;
        } else if (element.nodeName === "BUTTON" && /allprojects/.test(element.className)) {
            console.log("#####");
            // The user clicked on a <button> or clicked on an element inside a <button>
            showThisProject(element);
            break;
        } else if (element.nodeName === "BUTTON" && /joinProjectBtn/.test(element.className)) {
            // The user clicked on a <button> or clicked on an element inside a <button>
            joinThisProject((error, ping) => {
                console.log(ping);
            });
            break;
        } else if (element.nodeName === "BUTTON" && /editProjectBtn/.test(element.className)) {
            // The user clicked on a <button> or clicked on an element inside a <button>

            editThisProject(document.getElementById('projectID').innerText);
            break;
        } else if (element.nodeName === "A" && /userCVModal/.test(element.className)) {
            // The user clicked on a <a> or clicked on an element inside a <a>
            console.log(element);
            showThisUserCVModal(element);
            break;
        }
        else if (element.nodeName === "A" && /acceptThisUser/.test(element.className)) {
            // The user clicked on a <a> or clicked on an element inside a <a>
            console.log('Test Accept This User OK');
            acceptThisUser((error, element) => {
                console.log(element);
            });
            break;
        }
        element = element.parentNode;
    }
}



// Shows user project that was clicked
function showThisUserProject(button) {
    // do something with button
    getUserProjects((error, data) => {

        // creates elements for data of project objects
        let titlebox = document.createElement('p');
        let descriptionbox = document.createElement('p');
        let requirementsbox = document.createElement('p');
        let paymentbox = document.createElement('p');
        let devamountbox = document.createElement('p');
        let datebox = document.createElement('p');

        let projectIDbox = document.createElement('p');
        projectIDbox.setAttribute('id', 'projectID');

        let projectEditBtn = document.createElement('button');
        projectEditBtn.setAttribute('id', 'project-edit-btn');
        projectEditBtn.setAttribute('class', 'editProjectBtn btn btn-primary btn-xs');

        let tmpList = [];
        // renders to center page
        for (let i = 0; i < data.length; i++) {
            if (data[i]._id === button.id) {
                titlebox.innerHTML = "Title: " + data[i].title;
                descriptionbox.innerHTML = "Description: " + data[i].description;
                requirementsbox.innerHTML = "Requirements: " + data[i].requirements;
                paymentbox.innerHTML = "Payment: " + data[i].payment;
                devamountbox.innerHTML = "Developers required: " + data[i].devAmount;
                datebox.innerHTML = "Created: " + data[i].date + "<br><br>";
                projectIDbox.innerHTML = data[i]._id;
                projectEditBtn.innerHTML = "Edit This Project";

                document.getElementById('show-project').style.display = 'inline-block';
                document.getElementById('show-project-content').style.display = 'inline';
                document.getElementById('show-project-content').appendChild(titlebox).appendChild(descriptionbox).appendChild(requirementsbox).appendChild(paymentbox).appendChild(devamountbox).appendChild(datebox);
                document.getElementById('show-project-btns').appendChild(projectEditBtn).appendChild(projectIDbox);

                // Dropdownmenu list with pingd users
                let pingdListLabel = document.createElement('p');
                pingdListLabel.setAttribute('id', 'pingdlist');

                let pingFromUsersListOL = document.createElement('ol');
                pingFromUsersListOL.setAttribute('id', 'pingFromUsersListOL');

                for (let j = 0; j < data[i].devList.length; j++) {
                    let tmpContent = data[i].devList[j].firstname + " " + data[i].devList[j].lastname;
                    let tmpUser = data[i].devList[j].userId;
                    tmpList.push(tmpUser);
                    let pingFromUsersListLI = document.createElement('li');
                    pingdListLabel.innerHTML = "<h5>Developer Requests</h5>";
                    pingFromUsersListLI.innerHTML = "<a id='" + tmpUser + "' class='userCVModal' data-toggle='modal' data-target='#developerModal' style='cursor: pointer;'>" + tmpContent + "<p id='userIdCVModal' style='display: none;'>" + tmpUser + "</p></a>";
                    pingFromUsersListOL.appendChild(pingFromUsersListLI);
                }
                document.getElementById('requestlist').appendChild(pingdListLabel).appendChild(pingFromUsersListOL);

            }

        }
    });
    document.getElementById('show-project-content').innerHTML = "";
    document.getElementById('show-project-btns').innerHTML = "";
    document.getElementById('requestlist').innerHTML = "";
}

// dropdown btn show hide content
function dropdownShowHide() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

// Shows project that was clicked
function showThisProject(button) {
    document.getElementById('show-project-content').innerHTML = "";
    document.getElementById('show-project-btns').innerHTML = "";
    document.getElementById('requestlist').innerHTML = "";

    getAllProjects((error, data) => {
        // creates elements for data of project objects
        let formbox = document.createElement('form');
        formbox.setAttribute('action', '/dashboard');
        formbox.setAttribute('method', 'GET');

        let titlebox = document.createElement('p');
        titlebox.setAttribute('id', 'project-titlebox');
        let descriptionbox = document.createElement('p');
        descriptionbox.setAttribute('id', 'project-descriptionbox');
        descriptionbox.setAttribute('class', 'show-this-project-text');
        let requirementsbox = document.createElement('p');
        requirementsbox.setAttribute('id', 'project-requirementsbox');
        requirementsbox.setAttribute('class', 'show-this-project-text');
        let paymentbox = document.createElement('p');
        paymentbox.setAttribute('id', 'project-paymentbox');
        paymentbox.setAttribute('class', 'show-this-project-text');
        let devamountbox = document.createElement('p');
        devamountbox.setAttribute('id', 'project-devamountbox');
        devamountbox.setAttribute('class', 'show-this-project-text');
        let datebox = document.createElement('p');
        datebox.setAttribute('id', 'project-datebox');
        datebox.setAttribute('class', 'show-this-project-text');
        let horizontalRule = document.createElement('hr');
        let projectJoinBtn = document.createElement('button');
        projectJoinBtn.setAttribute('id', 'project-join-btn')
        projectJoinBtn.setAttribute('class', 'joinProjectBtn btn btn-primary btn-xs');

        let projectIDbox = document.createElement('p');
        projectIDbox.setAttribute('id', 'projectID');

        let requestbox = document.createElement('p');
        requestbox.setAttribute('id', 'projectBadge');
        requestbox.setAttribute('class', 'badge');


        let joinProjectLink = document.createElement('a');
        joinProjectLink.setAttribute('type', 'submit');

        // renders project to page center
        for (let i = 0; i < data.length; i++) {
            if (data[i]._id === button.id) {

                titlebox.innerHTML = data[i].title.toUpperCase();
                descriptionbox.innerHTML = "<strong>DESCRIPTION:</strong> " + data[i].description;
                requirementsbox.innerHTML = "<strong>REQUIREMENTS:</strong> " + beautifyArray(data[i].requirements);
                paymentbox.innerHTML = "<strong>PAYMENT:</strong> " + data[i].payment;
                devamountbox.innerHTML = "<strong>DEVELOPERS REQUIRED:</strong> " + data[i].devAmount;
                requestbox.innerHTML = "Developer Requests: " + data[i].devList.length;
                datebox.innerHTML = "<strong>CREATED:</strong> " + data[i].date;
                projectIDbox.innerHTML = data[i]._id;
                projectJoinBtn.innerHTML = "JOIN THIS PROJECT";

                projectJoinBtn.setAttribute('data-item', data[i]._id)

                document.getElementById('show-project').style.display = 'inline-block';
                let showProjectContent = document.getElementById('show-project-content');
                showProjectContent.style.display = 'inline';
                showProjectContent.appendChild(titlebox);
                showProjectContent.appendChild(descriptionbox);
                showProjectContent.appendChild(requirementsbox);
                showProjectContent.appendChild(paymentbox);
                showProjectContent.appendChild(devamountbox);
                showProjectContent.appendChild(datebox);
                showProjectContent.appendChild(horizontalRule);
                showProjectContent.appendChild(requestbox);
                //document.getElementById('show-project-btns').appendChild(projectJoinBtn);
                document.getElementById('show-project-btns').appendChild(formbox).appendChild(joinProjectLink).appendChild(projectJoinBtn).appendChild(projectIDbox);


            }
        }
    });

    joinProjectLink.setAttribute('href', '/dashboard/pingtoprojectowner/' + document.getElementById('projectID').innerHTML);
}

// Lägger till extra mellanslag mellan element i array för snyggare presentation
function beautifyArray(array) {
    if (array.length > 1) {
        let space = " ";
        for(let i = 1; i < array.length; i++) {
            array[i] = space.concat(array[i]);
        }
    }
    return array;
}

function joinThisProject(callback) {
    let error = null;
    $.ajax({
        method: 'GET',
        dataType: 'json',
        data: 'ping',
        jsonp: 'callback',
        url: '/dashboard/pingtoprojectowner/' + document.getElementById('projectID').innerHTML,
        success: function (ping) {
            callback(error, ping)
        }
    }).done(ping => {
        console.log(ping)
    }).catch(error => console.log(error));
}

function editThisProject(thisProjectID) {
    location.replace("/projects/editproject/" + thisProjectID);
    console.log(thisProjectID);
}

// render to modal
function thisUserCVModal(callback) {
    let error = null;
    $.ajax({
        method: 'GET',
        dataType: 'json',
        data: 'userCV',
        jsonp: 'callback',
        url: '/dashboard/usercvmodal/',
        success: function (userCV) {
            callback(error, userCV)
        }
    }).done(userCV => {
        console.log(userCV)
    }).catch(error => console.log(error));
}

// accepted user
function acceptThisUser(callback) {
    let error = null;

    $.ajax({
        method: 'GET',
        dataType: 'json',
        data: 'acceptUser',
        jsonp: 'callback',
        url: '/dashboard/useraccepted/' + document.getElementById('projectID').innerHTML,
        success: function (acceptUser) {
            callback(error, acceptUser)
        }
    }).done(acceptUser => {
        console.log(acceptUser)
    }).catch(error => console.log(error));
}

function showThisUserCVModal(userCVelement) {

    let tmpUserID = userCVelement.getAttribute('id');

    thisUserCVModal((error, data) => {
        console.log('Test modal data: ' + data);
        for (let i = 0; i < data.length; i++) {
            if (data[i]._id === tmpUserID) {
                document.getElementById('useracceptedBtn').setAttribute('href', '/dashboard/useraccepted/' + document.getElementById('projectID').innerHTML + "-" + tmpUserID);
                //document.getElementById('useracceptedBtn').setAttribute('data-item', tmpUserID);
                document.getElementById('developerCVImg').setAttribute('src', data[i].profileImage);
                document.getElementById('developerCVNameContent').innerHTML = "Name: " + data[i].firstname + " " + data[i].lastname + "<br> Nick Name: " + data[i].userNickName + "<br>";
                document.getElementById('developerCVContent').innerHTML = "Company: " + data[i].companyName + "<br> Webpage: " + data[i].webpage + "<br> Email: " + data[i].email + "<br> Phonenr: "
                    + data[i].phonenr + "<br> Competence: " + data[i]._id;
            }
        }

    });
}



// render EJS
