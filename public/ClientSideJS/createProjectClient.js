
//------------ Create competence tags --------------------------
const tagContainer = document.querySelector('.tag-container-project');
const input = document.querySelector('#requirements-input');
const createBtn = document.querySelector('#create-btn');
const hiddenInput = document.querySelector('#hidden-input');

let tags = [];

function createTags(label) {

    const div = document.createElement('div');
    div.setAttribute('class', 'tag col-sm-1');
    div.setAttribute('name', 'tag');

    const span = document.createElement('span');
    span.setAttribute('class', 'tag-text')
    span.innerHTML = label;

    const closeBtn = document.createElement('i');
    closeBtn.setAttribute('class', 'material-icons glyphicon glyphicon-remove');
    closeBtn.setAttribute('data-item', label);

    div.appendChild(span);
    div.appendChild(closeBtn);
    return div;
}

// resets tags array
function resetTags() {
    document.querySelectorAll('.tag').forEach(function (tag) {
        tag.parentElement.removeChild(tag);
    });
}

// adds tags to array
function addTags() {
    resetTags();
    tags.slice().reverse().forEach(function (tag) {
        const input = createTags(tag);
        tagContainer.prepend(input);
    });
}

// eventlistener for input
input.addEventListener('keyup', function (e) {
    if (e.keyCode === 13) {
        tags.push(input.value);
        addTags();
        input.value = "";
    }
    e.preventDefault();
    return false;
});

// eventlistener for tag to close
document.addEventListener('click', function (e) {
    if (e.target.tagName === 'I') {
        const value = e.target.getAttribute('data-item');
        const index = tags.indexOf(value);
        tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
        addTags();
    }
});

document.addEventListener('click', function (e) {
    var create = "create-btn";
    var save = "save-btn";
    if ((e.target.id === create) || (e.target.id === save)) {
        hiddenInput.value = tags;
    } else {
        console.log('Something went wrong with tags array');
    }
})