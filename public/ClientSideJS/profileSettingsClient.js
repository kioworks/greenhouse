
var tags = [];

(function () {
    var oldTagsElements = document.getElementsByClassName('tag');

    for (let item of oldTagsElements) {
        tags.push(item.firstElementChild.innerHTML);
    }
    for (let i = 0; tags.length > i; i++) {
        tags[i] = tags[i].trim();
    }
})();

const hiddenInput = document.getElementById('hidden-input');
const submitBtn = document.getElementById('submit-btn')

//-------------- Outsourcer checkbox toggle ------------------------
function outsourcerCheckbox() {
    let outsourcerCheckBox = document.getElementById('profileTypeOutsourcer').checked;
    let outsourcerCreateProjectBtn = document.getElementById('createProjectBtn');

    if (outsourcerCheckBox == true) {
        //outsourcerCreateProjectBtn.style.display = 'inline';
    } else if (outsourcerCheckBox !== true) {
        outsourcerCreateProjectBtn.style.display = "none";
    }
}
//------------ End of checkbox toggle code --------------------

//-------------- Dev checkbox toggle ------------------------
function devCompetenceCheckbox() {
    let devCheckBox = document.getElementById('profileTypeDeveloper').checked;
    let devMatchMeBtn = document.getElementById('matchUserBtn');
    let devCompetenceContainer = document.getElementById('container-tags-input');

    if (devCheckBox == true) {
        devCompetenceContainer.style.display = 'inline';
        //devMatchMeBtn.style.display = 'inline';
    } else if (devCheckBox !== true) {
        devCompetenceContainer.style.display = 'none';
        devMatchMeBtn.style.display = 'none';
    }
}
//------------ End of checkbox toggle code --------------------

//------------ Create competence tags --------------------------
const tagContainer = document.querySelector('.tag-container');
const input = document.querySelector('.tag-container-user input');

function createTags(label) {
    const div = document.createElement('div');
    div.setAttribute('class', 'tag col-sm-1');

    const span = document.createElement('span');
    span.setAttribute('id', 'span-tag-text');
    span.innerHTML = label;

    const closeBtn = document.createElement('i');
    closeBtn.setAttribute('class', 'material-icons glyphicon glyphicon-remove'); //Lägger till ett glyphicon X
    closeBtn.setAttribute('data-item', label);
    closeBtn.setAttribute('id', 'tag-btn');


    div.appendChild(span);
    div.appendChild(closeBtn);

    return div;
}

// resets tags array
function resetTags() {
    document.querySelectorAll('.tag').forEach(function (tag) {
        tag.parentElement.removeChild(tag);
    });
}

// adds tags to array
function addTags() {
    resetTags();
    tags.slice().reverse().forEach(function (tag) {
        const input = createTags(tag);
        tagContainer.prepend(input); // lägger till tagen i div med class tags-container inte tag
    })
}

// eventlistener for input
input.addEventListener('keyup', function (e) {

    if (e.keyCode === 13 && input.value !== " ") {
        tags.push(input.value);
        addTags();
        input.value = "";
    }
    e.preventDefault();
    return false;
})

// eventlistener for tag to close
document.addEventListener('click', function (e) {

    if (e.target.id === 'tag-btn') {
        const value = e.target.getAttribute('data-item');
        const index = tags.indexOf(value);
        tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
        addTags();
    }

    if (e.target.id === 'submit-btn') {
        if (tags.length > 0) {

            hiddenInput.value = tags;
        } else hiddenInput.value = null;
    }
});



    //------------------------- End of tags code ------------------------------